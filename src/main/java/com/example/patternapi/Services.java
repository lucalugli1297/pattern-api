package com.example.patternapi;


import com.google.gson.Gson;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class Services {
    PointLab pointServices = new PointLab();
        @GetMapping("/space")
    public String spaceGet() {
        ArrayList<Point> listPoint = pointServices.getAllPoint();
        Gson gson = new Gson();
        String jsonCartList = gson.toJson(listPoint);
        return jsonCartList;
    }

    @PostMapping("/point")
    public String point(@RequestParam(name = "x") String x, @RequestParam(name = "y")String y) {
        if(StringUtils.isBlank(x) || StringUtils.isBlank(y) || !NumberUtils.isCreatable(x)|| !NumberUtils.isCreatable(y)){
            return "Validation error";
        }
        Point point = new Point(Double.parseDouble(x),Double.parseDouble(y));
        ArrayList <Point> listPoint = pointServices.addPoint(point);
        Gson gson = new Gson();
        String jsonCartList = gson.toJson(listPoint);
        return jsonCartList;
    }
    @DeleteMapping("/space")
    public void space() {
        pointServices.delete();
    }
    @GetMapping("/lines/{n}")
    public String getAllLines(@PathVariable String n){
        if(StringUtils.isBlank(n) || !NumberUtils.isCreatable(n) || Integer.parseInt(n)<2)
        {
            return "Validation error";
        }
        ArrayList<ArrayList<Point>>  listPoint = pointServices.getAllLinesContainsN(Integer.parseInt(n));
        Gson gson = new Gson();
        String jsonCartList = gson.toJson(listPoint);
        return jsonCartList;
    }


}
