package com.example.patternapi;

import com.google.gson.Gson;

import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PointLab {

    ArrayList<Point> listPoint= new ArrayList<>();

    public ArrayList<Point> getAllPoint (){
        return listPoint;
    }
    public ArrayList<Point>  addPoint(Point point) {
        if(!listPoint.stream().anyMatch(point::equals)) {
            listPoint.add(point);
        }
        return listPoint;
    }

    public void delete() {
        listPoint.clear();
    }

    public ArrayList<ArrayList<Point>>  getAllLinesContainsN(int n) {
        ArrayList<ArrayList<Point>>  returnLines =  new ArrayList<>();
        ArrayList<Line> listAllLine = new ArrayList<>();
        ArrayList<Line> listLineFound = new ArrayList<>();
        //calculate all possible lines
        for (int i = 0; i < listPoint.size(); i++) {
            for (int j = i+1; j < listPoint.size(); j++) {
                Line lineAdd = new Line(listPoint.get(i), listPoint.get(j));
                listAllLine.add(lineAdd);
                if(listAllLine.stream().filter(lineAdd::equals).collect(Collectors.toList()).size()>=getLineNumber(n) && listLineFound.stream().noneMatch(lineAdd::equals) && !(lineAdd.getQ()==0 && lineAdd.getM()==0)){
                    listLineFound.add(lineAdd);
                }
            }
        }
        //find line on same y-axis and diagonal
        getAllPointSameDiagonal(returnLines, listAllLine, listLineFound);
        //find line on same x-axis
        getAllPointSameX(returnLines, n);
        return returnLines;
    }

    private void getAllPointSameX(ArrayList<ArrayList<Point>> returnLines, int n) {
        List <Double> pointX = new ArrayList<>();
        ArrayList<Point> singleLine = new ArrayList<>();

        for (Point point : listPoint) {

            List<Point> filteredPoints = listPoint.stream().filter(p-> { return p.getX()== point.getX();}).collect(Collectors.toList());
            if(filteredPoints.size()>=n && pointX.stream().noneMatch(p -> { return p == point.getX();})) {
                singleLine = new ArrayList<>();
                pointX.add(point.getX());
                for(Point p : filteredPoints) {
                    singleLine.add(p);
                }
                returnLines.add(singleLine);
            }
         }
    }

    private void getAllPointSameDiagonal(ArrayList<ArrayList<Point>> returnLines, ArrayList<Line> listAllLine, ArrayList<Line> listLineFound) {
        ArrayList<Point> singleLine= new ArrayList<>();
        for (Line line : listLineFound) {
            singleLine = new ArrayList<>();
            List<Line> filteredLines = listAllLine.stream().filter(line::equals).collect(Collectors.toList());
            for (Line filteredLine : filteredLines) {
                if(singleLine.stream().noneMatch(filteredLine.getFistPoint()::equals)){
                    singleLine.add(filteredLine.getFistPoint());
                }
                if(singleLine.stream().noneMatch(filteredLine.getSecondPoint()::equals)){
                    singleLine.add(filteredLine.getSecondPoint());
                }
            }
            returnLines.add(singleLine);
        }
    }

    public  int getLineNumber(int n){
        int m=0;
        int i=1;
        while(i<n){
            m=m+i;
            i=i+1;
        }
        return m;
    }

}
