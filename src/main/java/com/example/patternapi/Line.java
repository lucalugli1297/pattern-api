package com.example.patternapi;

public class Line {

    Point fistPoint;
    Point secondPoint;
    double m;
    double q;

    Line (Point firstPoint, Point secondPoint ){
        this.fistPoint = firstPoint;
        this.secondPoint= secondPoint;
        if(firstPoint.getX()-secondPoint.getX()!=0){
            this.m = (firstPoint.getY() - secondPoint.getY()) / (firstPoint.getX() - secondPoint.getX());
            this.q = (firstPoint.getX() * secondPoint.getY() - (firstPoint.getY() * secondPoint.getX())) / (firstPoint.getX() - secondPoint.getX());
        }
    }

    public boolean equals (Line line){
        return this.m== line.getM() && this.q== line.getQ();
    }
    public double getM() {
        return m;
    }

    public void setM(double m) {
        this.m = m;
    }


    public double getQ() {
        return q;
    }

    public void setQ(double q) {
        this.q = q;
    }
    public Point getFistPoint() {
        return fistPoint;
    }

    public void setFistPoint(Point fistPoint) {
        this.fistPoint = fistPoint;
    }
    public Point getSecondPoint() {
        return secondPoint;
    }

    public void setSecondPoint(Point secondPoint) {
        this.secondPoint = secondPoint;
    }

}
